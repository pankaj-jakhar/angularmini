export const environment = {
  production: true,
  API_ENDPOINT:"https://angular-mini.herokuapp.com/",
  APIPATH:{
    TASKS:"tasks"
  }
};
