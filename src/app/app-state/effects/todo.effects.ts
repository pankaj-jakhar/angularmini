import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, exhaustMap, catchError } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import * as todoActions from '../actions';

@Injectable()
export class TodoEffects {

  constructor(
    private actions$: Actions,
    private todoService: ApiService
  ) {}

  getTasks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.getTasks),
      exhaustMap(action =>
        this.todoService.getAll({skip:0,limit:20,order:{"index":-1,"updatedAt":-1}}).pipe(
          map(response => {
            console.log("response:::", response)
            return todoActions.getTasksSuccess({response})
          }),
          catchError((error: any) => of(todoActions.getTasksFailure(error))))
      )
    )
  );

  search$ = createEffect(() =>
  this.actions$.pipe(
    ofType(todoActions.search),
    exhaustMap(action =>
      this.todoService.getAllSearch(action).pipe(
        map(response => {
          console.log("response:::", response)
          return todoActions.searchSuccess({response})
        }),
        catchError((error: any) => of(todoActions.searchFailure(error))))
    )
  )
);

pagination$ = createEffect(() =>
  this.actions$.pipe(
    ofType(todoActions.pagination),
    exhaustMap(action =>
      this.todoService.getAllSearch(action).pipe(
        map(response => {
          console.log("response:::", response)
          return todoActions.paginationSuccess({response})
        }),
        catchError((error: any) => of(todoActions.paginationFailure(error))))
    )
  )
);

  createTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.createTask),
      exhaustMap(action =>
        this.todoService.add(action)
        .pipe(
          map(response => todoActions.createTaskSuccess(response)),
          catchError((error: any) => of(todoActions.createTaskFailure(error))))
      )
    )
  );


  deleteTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.deleteTask),
      exhaustMap(action => this.todoService.deleteById(action).pipe(
          map(response => todoActions.deleteTaskSuccess(response)),
          catchError((error: any) => of(todoActions.deleteTaskFailure(error))))
      )
    )
  );

  editTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.editTask),
      exhaustMap(action =>
        this.todoService.edit(action).pipe(
          map(response => todoActions.editTaskSuccess(response)),
          catchError((error: any) => of(todoActions.editTaskFailure(error))))
      )
    )
  );

  count$ = createEffect(() =>
  this.actions$.pipe(
    ofType(todoActions.count),
    exhaustMap(action =>
      this.todoService.count(action).pipe(
        map(response => todoActions.countSuccess(response)),
        catchError((error: any) => of(todoActions.countFailure(error))))
    )
  )
);

}
