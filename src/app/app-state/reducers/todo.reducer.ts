import { Action, createReducer, on } from '@ngrx/store';
import { Task } from '../entity';
import * as todoActions from '../actions';
import * as _ from 'lodash'
import * as storage from '../state/storage';

export interface State {
  tasks?: Task[];
  allTasks?:[];
  eventTasks?:[];
  eventObj?:any[];
  total?:any;
  eventTotal?:any;
  deleteTaskId?: any;
  result?: any;
  isLoading?: boolean;
  isEvent?: boolean;
  isLoadingSuccess?: boolean;
  isLoadingFailure?: boolean;
}

export const initialState: State =storage.getItem('todo')

const todoReducer = createReducer(
  initialState,

  // GeTasks
  on(todoActions.getTasks, (state) => ({...state, isLoading: true})),
  on(todoActions.getTasksSuccess, (state, result) => {
    return {
      ...state,
      allTasks: result.response,
      skip:20, 
      isLoading: false, 
      isLoadingSuccess: true
    }
  }),

   // GETCount
   on(todoActions.count, (state) => ({...state, isLoading: true})),
   on(todoActions.countSuccess, (state, result) => {
     console.log(result)
     return {
       ...state,
       total:  !state.isEvent ? result.count : state.total,
       eventTotal: state.isEvent ? result.count : state.eventTotal,
       isLoading: false, 
       isLoadingSuccess: true
     }
   }),

  on(todoActions.clearTasks, (state, result) => {
    console.log("state>>>>>>>>>>>>>>",state,result)
    
    return {
      ...state,
      tasks:state.allTasks,
      isEvent:false,
      eventSkip:0,
      eventObj:[],
      eventTasks:[],
      isLoading: false,
      isLoadingSuccess: true
    };
  }),

  on(todoActions.search, (state, task) => ({...state, isLoading: true,isEvent:true,eventObj:task})),
  on(todoActions.searchSuccess, (state, result) => {
    console.log("state>>>>>>>>>>>>>>",state,result)
   
    return {
      ...state,
      eventTasks:result.response,
      eventSkip:20, 
      isLoading: false,
      isLoadingSuccess: true
    };
  }),

  // Create Task Reducers
  on(todoActions.createTask, (state, {task}) => ({...state, isLoading: true})),
  on(todoActions.createTaskSuccess, (state, result) => {
    console.log("state>>>>>>>>>>>>>>",state,result)
    const allTasks = undefined !== state.tasks ? _.cloneDeep(state.allTasks) : [];
    allTasks.unshift(result);
    return {
      ...state,
      allTasks,
      isLoading: false,
      isLoadingSuccess: true
    };
  }),

  // Delete Task Reducers
  on(todoActions.deleteTask, (state, task) => ({...state, isLoading: true, deleteTaskId: task.id})),
  on(todoActions.deleteTaskSuccess, (state, result) => {
    let allTasks = undefined !== state.allTasks ? _.cloneDeep(state.allTasks) : [];
    allTasks = allTasks.filter(task => task._id !== state.deleteTaskId);
    return {
      ...state,
      allTasks,
      isLoading: false,
      isLoadingSuccess: true
    };
  }),

   // Edit Task Reducers
   on(todoActions.editTask, (state, task) => ({...state, isLoading: true})),
   on(todoActions.editTaskSuccess, (state, result) => {
    let allTasks = undefined !== state.allTasks ? _.cloneDeep(state.allTasks) : [];
    allTasks = allTasks.map(tsk => {
      if (tsk._id === result._id) {
        tsk = result;
      }
      return tsk;
    }).sort((a,b) => (a.index < b.index) ? 1 : ((b.index < a.index) ? -1 : 0));
    return {
      ...state,
      allTasks,
      isLoading: false,
      isLoadingSuccess: true
    };
  }),


  // Pagination Task Reducers
  on(todoActions.pagination, (state, task) => ({...state, isLoading: true})),
  on(todoActions.paginationSuccess, (state:any, result:any) => {
  
   return {
     ...state,
     allTasks: !state.isEvent ? [...state.allTasks,...result.response] : state.allTasks,
     eventTasks: state.isEvent? [...state.eventTasks,...result.response] : state.eventTasks,
     isLoading: false,
     skip: !state.isEvent? state.skip + 20 :state.skip,
     eventSkip: state.isEvent? state.eventSkip + 20 :state.eventSkip,
     isLoadingSuccess: true
   };
 })
);

export function reducer(state: State | undefined, action: Action): any {
  return todoReducer(state, action);
}

export const getTasks = (state: State) => {
  console.log(state)
  let tasks
  if(state.isEvent){
    tasks = state.eventTasks
  }else{
    tasks =state.allTasks
  }
  return {
    ...state,tasks}
};
