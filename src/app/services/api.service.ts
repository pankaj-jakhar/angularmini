import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl : string = environment.API_ENDPOINT;
	headers : any;

  constructor(
    public http: HttpClient,
  ) { 
    this.headers = new Headers();
		this.headers.append('Access-Control-Allow-Origin' , '*');
		this.headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
		this.headers.append('Accept','application/json');
		this.headers.append('content-type','application/json');
  }

  getById(id,filterJSON ={}){
		let filter = encodeURI(JSON.stringify(filterJSON));
		let profileURL = this.apiUrl + environment.APIPATH.TASKS+ '/' + id + '?filter=' + filter;
		return new Promise((resolve, reject) => {
			this.http.get(profileURL).subscribe(res => {
		        resolve(res);
			},(err)=>{
          reject(err);
			});
		});
  }

  getAll(filterJSON:any ={}){
    let filter = encodeURIComponent(JSON.stringify(filterJSON));
		let profileURL = this.apiUrl +environment.APIPATH.TASKS +'?filter=' + filter;
    return this.http.get(profileURL);
		// return new Promise((resolve, reject) => {
		// 	this.http.get(profileURL).subscribe(res => {
		//         resolve(res);
		// 	},(err)=>{
	  //       	reject(err);
		// 	});
		// });
  }

  getAllSearch(filterJSON:any ={}){
    let filter = encodeURIComponent(JSON.stringify(filterJSON));
		let profileURL = this.apiUrl +environment.APIPATH.TASKS +'?filter=' + filter;
    return this.http.get(profileURL);
		// return new Promise((resolve, reject) => {
		// 	this.http.get(profileURL).subscribe(res => {
		//         resolve(res);
		// 	},(err)=>{
	  //       	reject(err);
		// 	});
		// });
  }

  add(data) {
    console.log(data);
    let signupUrl = this.apiUrl + environment.APIPATH.TASKS;
    return  this.http.post(signupUrl, data,{headers:this.headers})
    // return new Promise((resolve, reject) => {
    //   this.http.post(signupUrl, data,{headers:this.headers})
    //     .subscribe(res => {
    //         // this.dialog.openContentDialog(AppSettings.MESSAGE.USER.ADD_TITLE,AppSettings.MESSAGE.USER.ADD);
    //       resolve(res);
    //     }, (err) => {
    //       reject(err);
    //     });
    // });
  }

  edit(userInfo) {
    console.log(userInfo)
    let editUserUrl = this.apiUrl + environment.APIPATH.TASKS + '/' + userInfo._id ;
    return this.http.patch(editUserUrl, userInfo,{headers:this.headers});
  // return new Promise((resolve, reject) => {
  //   this.http.patch(editUserUrl, userInfo,{headers:this.headers})
  //     .subscribe(res => {
  //       resolve(res);
  //     }, (err) => {
  //       reject(err);
  //     });
  // });
}

editAll(userInfo,filterJSON = {}) {
  let filter = encodeURI(JSON.stringify(filterJSON));
  let editUserUrl = this.apiUrl + environment.APIPATH.TASKS+'?filter=' + filter;
  return this.http.patch(editUserUrl, userInfo,{headers:this.headers}) 
// return new Promise((resolve, reject) => {
//   this.http.patch(editUserUrl, userInfo,{headers:this.headers})
//     .subscribe(res => {
//       resolve(res);
//     }, (err) => {
//       reject(err);
//     });
// });
}

deleteById(id){
  console.log(id)
  let deleteUserUrl = this.apiUrl + environment.APIPATH.TASKS +'/' + id.id  ;
  return this.http.delete(deleteUserUrl,{headers:this.headers})
  // return new Promise((resolve, reject) => {
  //     .subscribe(res => {
  //       resolve(res);
  //     }, (err) => {
  //       reject(err);
  //   });
  // })
}


count(filterJSON ={}) {
  let filter = encodeURI(JSON.stringify(filterJSON));
  let signupUrl = this.apiUrl + environment.APIPATH.TASKS+'/count'+'?filter=' + filter;
  return this.http.get(signupUrl)
  // return new Promise((resolve, reject) => {
  //     .subscribe(res => {
  //       resolve(res);
  //     }, (err) => {
  //       reject(err);
  //     });
  // });
}
}
