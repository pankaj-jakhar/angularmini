import { Component, HostListener, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { Store } from '@ngrx/store';
import * as todoActions from '../../app-state/actions';
import * as fromRoot from '../../app-state';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  addFlag:boolean = false;
 
  allTasks:any;
  destroy$: Subject<boolean> = new Subject<boolean>();
  searchValue:any ="";
  todoData:any;
  notcheck:boolean = false;
  constructor(
    private readonly store: Store,
    public snackBar: MatSnackBar
  ) { 
    this.store.dispatch(todoActions.getTasks())
    this.store.dispatch(todoActions.count({}))
    this.getAllTasks()
  }

  ngOnInit(): void {
  }
  
  getAllTasks(){
    console.log("new")
    this.store.select(fromRoot.getTasks).pipe(
      takeUntil(this.destroy$)
    ).subscribe(data =>{ 
      console.log(data)
      this.todoData = JSON.parse(JSON.stringify(data));
      if(data.tasks){
        this.allTasks = JSON.parse(JSON.stringify(data.tasks)) 
        if(this.todoData.isEvent){
          if(this.todoData.eventObj.where && this.todoData.eventObj.where.title['$regex']){
            this.searchValue = this.todoData.eventObj.where.title['$regex']
          }
        }
      }
    });
  }

  drop(event: CdkDragDrop<any[]>) {
    console.log(event)
    console.log(event.previousIndex, event.currentIndex,event.previousIndex == event.currentIndex)
    if(!this.todoData.isEvent){
      let array = JSON.parse(JSON.stringify(this.allTasks));
      let value1 = array[event.previousIndex];
      array.splice(event.previousIndex,1);
      array.splice(event.currentIndex,0,value1);
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      if(!(event.previousIndex == event.currentIndex)){
        let index = 0;
        if(event.currentIndex == 0){
          index = array[event.currentIndex + 1].index +1;
        }
        if(event.currentIndex !== 0){
          if(array[event.currentIndex - 1] && array[event.currentIndex + 1]){
            index = (array[event.currentIndex - 1].index + array[event.currentIndex + 1].index)/2
          }else{
            index = array[event.currentIndex - 1].index -1 ;
          }
        }
        console.log(array,index);
        let data :any = {
          "index":index,
          "_id":event.item.element.nativeElement.id
        } 
        this.destroy$.next(true);
        this.store.dispatch(todoActions.editTask(data));
        setTimeout(() => {
          this.destroy$.next(false);
          this.getAllTasks();
        }, 2000);
      }
    }else{
        this.snackBar.open('Drag and Drop is Disabled', 'close',{
          panelClass: ['style-error'],
          duration : 5000
        });
    }
  }

 
  
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
  completeTask(task){
    console.log(task);
    let data :any = {
      "statusValue":"Complete",
      "date_closed":new Date(),
      "_id":task._id
    }
    this.store.dispatch(todoActions.editTask(data));
  }

  selectionChange(task,value){
    console.log(value)
    let data :any = {
      "statusValue":value,
      "_id":task._id
    }
    if(value == "Complete"){
      data.date_closed = new Date()
    }else{
      data.date_closed = null
    }
    this.store.dispatch(todoActions.editTask(data));
  }

  handleDOBChange(task,date,type){
    console.log(task,date,type)
    let data :any = {
      "_id":task._id
    }
    if(type == "start"){
        data.start_date = date.value
    }else{
        data.due_date = date.value
    }
    this.store.dispatch(todoActions.editTask(data));
  }

  editTitle(task){
    let data :any = {
      "title":task.title,
      "_id":task._id
    }
    task.edit = false;
    this.store.dispatch(todoActions.editTask(data));
  }

  openEditBox(task){
    if(task.statusValue !== "Complete"){
      task.edit = true;
      setTimeout(() => {
        document.getElementById("editInput"+task._id).focus() 
      }, 100);
    }
  }

  addEvent(value){
    console.log("it is called",value)
    this.addFlag = !this.addFlag
  }
 
  deleteTask(task){
    this.store.dispatch(todoActions.deleteTask({"id":task._id}))
  }


  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
  //In chrome and some browser scroll is given to body tag
  let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
  let max = document.documentElement.scrollHeight;
  // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
  console.log("here",pos,max)
  if(pos > (max - 30) )   {
    console.log("here i m ")
    if(this.todoData.isEvent){
      if(this.todoData.eventSkip == this.allTasks.length){
          console.log("here i m 1")
          let obj:any = {};
          if(this.todoData.eventObj){
            obj = this.todoData.eventObj
          }
          obj.limit = 20;
          obj.skip = this.todoData.eventSkip;
          if(!(obj.order &&  Object.keys(obj.order).length > 0)){
            obj.order = {index:-1}
          }
        if(!this.todoData.isLoading){
              this.store.dispatch(todoActions.pagination(obj))
          }
        }
    }else{
      if(this.todoData.skip == this.allTasks.length){
        console.log("here i m 1")
        let obj:any = {};
        obj.limit = 20;
        obj.skip = this.todoData.skip;
        obj.order = {index:-1}
        if(!this.todoData.isLoading){
              this.store.dispatch(todoActions.pagination(obj))
        }
      }
    }
  }
  }

}
