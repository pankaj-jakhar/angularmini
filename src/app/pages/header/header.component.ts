import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as fromRoot from '../../app-state';
import * as todoActions from '../../app-state/actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() addEvent: EventEmitter<any> = new EventEmitter();
  destroy$: Subject<boolean> = new Subject<boolean>();
  addFlag:boolean = false;
  todoData:any;
  constructor(
    private readonly store: Store
    ) {
      this.getAllTasks()
     }

     getAllTasks(){
      console.log("new")
      this.store.select(fromRoot.getTasks).pipe(
        takeUntil(this.destroy$)
      ).subscribe(data =>{ 
        console.log(data)
        this.todoData = JSON.parse(JSON.stringify(data));
      });
    }

  ngOnInit(): void {
  }

  sort(field,orderBy){
    console.log(field,orderBy)
    let obj:any ={};
    if(this.todoData.eventObj){
      obj = this.todoData.eventObj
    }
    if(!(obj.order && Object.keys(obj.order).length > 0)){
      console.log("in avalue")
      obj.order = {}
      obj.order[field] = orderBy
    }else{
      obj.order[field] = orderBy
    }
    obj.limit = 20;
    obj.skip = 0;
    this.store.dispatch(todoActions.search(obj))
  }
  removeSort(field){
    let obj:any ={};
    if(this.todoData.eventObj){
      obj = this.todoData.eventObj
    }
    delete obj.order[field];
    if(Object.keys(obj.order).length > 0){
      console.log("in avalue")
      obj.limit = 20;
      obj.skip = 0;
      this.store.dispatch(todoActions.search(obj))
    }else{
      delete obj.order;
      delete obj.type
      delete obj.limit
      delete obj.skip
      if(Object.keys(obj).length == 0){
        console.log("empty")
        this.store.dispatch(todoActions.clearTasks())
      }else{
        obj.limit = 20;
        obj.skip = 0;
        this.store.dispatch(todoActions.search(obj))
      }
    }
  }

  checkForOrder(field){
    return this.todoData.eventObj && this.todoData.eventObj.order 
    ?  
    Object.keys(this.todoData.eventObj.order).indexOf(field)
    : 
    -1
  }

  checkForOrderType(field){
    return this.todoData.eventObj && this.todoData.eventObj.order 
    ?  
    this.todoData.eventObj.order[field]
    : 
    0
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
  addEventFun(){
    this.addEvent.emit(true)
  }

}
