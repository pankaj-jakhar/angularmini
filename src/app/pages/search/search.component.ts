import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as todoActions from '../../app-state/actions';
import * as fromRoot from '../../app-state';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  destroy$: Subject<boolean> = new Subject<boolean>();
  searchValue:any ="";
  todoData:any;

  constructor(
    private readonly store: Store
  ) {
    this.getAllTasks()

   }

  getAllTasks(){
    console.log("new")
    this.store.select(fromRoot.getTasks).pipe(
      takeUntil(this.destroy$)
    ).subscribe(data =>{ 
      console.log(data)
      this.todoData = JSON.parse(JSON.stringify(data));
    });
  }


  ngOnInit(): void {
  }

  search(event){
    let obj:any = {}
    if(this.todoData.eventObj){
      obj = this.todoData.eventObj
    }
    if(this.searchValue !== ""){
      console.log("in avalue")
      obj.where = {title:{"$regex":this.searchValue,"$options": 'i'}}
      this.store.dispatch(todoActions.count({"where":{title:{"$regex":this.searchValue,"$options": 'i'}}}))
    }else{
      delete obj.where
      delete obj.type
      delete obj.skip
      delete obj.limit
      console.log("in elses",obj)
    }
    if(Object.keys(obj).length == 0){
      console.log("empty")
      this.store.dispatch(todoActions.clearTasks())
    }else{
      console.log(this.searchValue)
      this.store.dispatch(todoActions.search(obj))
    }
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
