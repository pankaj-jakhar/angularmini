import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as todoActions from '../../app-state/actions';
import * as fromRoot from '../../app-state';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  @Output() addEvent: EventEmitter<any> = new EventEmitter();
  newTask:any={
    "title":"",
    "statusValue":"To do",
    "due_date":"",
    "start_date":"",
    "date_closed":""
  }
  constructor(
    private readonly store: Store
  ) { }

  ngOnInit(): void {
  }
  createTask(){
    console.log(this.newTask);
    this.store.dispatch(todoActions.createTask(this.newTask));
      this.newTask = {
        "title":"",
        "statusValue":"To do",
        "due_date":"",
        "start_date":"",
        "date_closed":""
      }
  }

  removeValues(){
    this.newTask = {
      "title":"",
      "statusValue":"To do",
      "due_date":"",
      "start_date":"",
      "date_closed":""
    }
    this.addEvent.emit(true)
  }

}
